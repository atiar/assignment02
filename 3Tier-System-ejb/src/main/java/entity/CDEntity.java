/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="CDs")
@DiscriminatorValue("CD")
@NamedQueries(
        {
            @NamedQuery(name = "CD.findAll", query = "SELECT c FROM CDEntity c"),
            @NamedQuery(name = "CD.findByCDsTitle", query = "SELECT c FROM CDEntity c WHERE c.title = :title")
        })
public class CDEntity extends ProductEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "genre")
    private String genre;
    @Column(name = "singer")
    private String singer;

    public String getGenre() {
        return genre;
    }

    public String getSinger() {
        return singer;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CDEntity)) {
            return false;
        }
        CDEntity other = (CDEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CD[ id=" + id + " ]";
    }
}
