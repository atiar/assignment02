/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="Books")
@DiscriminatorValue("Book")
@NamedQueries(
        {
            @NamedQuery(name = "Book.findAll", query = "SELECT b FROM BookEntity b"),
            @NamedQuery(name = "Book.findByBooksISBN", query = "SELECT b FROM BookEntity b WHERE b.isbn = :isbn")
        })
public class BookEntity extends ProductEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name="isbn")
    private String isbn;
    
    @Column(name="author")
    private String author;
    
    public String getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BookEntity)) {
            return false;
        }
        BookEntity other = (BookEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Book[ id=" + id + " ]";
    }
    
}
