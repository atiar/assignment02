/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entity.BookEntity;
import entity.CDEntity;
import entity.CustomerEntity;
import entity.OrderEntity;
import entity.ProductEntity;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class SessionBean {

    @PersistenceContext(unitName = "kz_eSystem-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    public List<CustomerEntity> getCustomers() {
        Query query = em.createNamedQuery("Customer.findAll");
        return query.getResultList();
    }

    public List<CustomerEntity> getCustomersByName(String name) {
        if (name == null || name.length() == 0 || name.equals("")) {
            return getCustomers();
        }
        Query query = em.createNamedQuery("Customer.findByCustomersName").setParameter("name", name);
        return query.getResultList();
    }
    public CustomerEntity getCustomerById(Long id) {
        Query query = em.createNamedQuery("Customer.findByCustomersId").setParameter("id", id);
        return (CustomerEntity) query.getResultList().get(0);
    }
    public List<BookEntity> getBooks() {
        Query query = em.createNamedQuery("Book.findAll");
        return query.getResultList();
    }
    public List<BookEntity> getBooksByISBN(String isbn) {
        if (isbn == null || isbn.length() == 0 || isbn.equals("")) {
            return getBooks();
        }
        Query query = em.createNamedQuery("Book.findByBooksISBN").setParameter("isbn", isbn);
        return query.getResultList();
    }
    public List<CDEntity> getCDs() {
        Query query = em.createNamedQuery("CD.findAll");
        return query.getResultList();
    }
    public List<CDEntity> getCDsByTitle(String title) {
        if (title == null || title.length() == 0 || title.equals("")) {
            return getCDs();
        }
        Query query = em.createNamedQuery("CD.findByCDsTitle").setParameter("title", title);
        return query.getResultList();
    }
    public List<OrderEntity> getOrders() {
        Query query = em.createNamedQuery("Order.findAll");
        return query.getResultList();
    }
    public List<OrderEntity> getOrdersById(Long id) {
        if (id == null || id==0) {
            return getOrders();
        }
        Query query = em.createNamedQuery("Order.findByOrdersId").setParameter("id", id);
        return query.getResultList();
    }
    public List<ProductEntity> getProducts() {
        Query query = em.createNamedQuery("Product.findAll");
        return query.getResultList();
    }
    public ProductEntity getProductById(Long id) {
        Query query = em.createNamedQuery("Product.findByProductsId").setParameter("id", id);
        return (ProductEntity) query.getResultList().get(0);
    }
    public void save(Object object) {
        em.persist(object);
    }

    public void update(Object object) {
        em.merge(object);
    }

    public void delete(Object object) {
        em.remove(em.merge(object));
    }
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
